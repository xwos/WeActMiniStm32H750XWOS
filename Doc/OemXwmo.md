增加OEM模块的方法
--------

# 设置OEM模块的路径

打开 `XWOS/xwbd/WeActMiniStm32H750/cfg/project.h` ,
`XWCFG_OEMPATH` 定义的路径即为 **OEM模块的路径** 。

**OEM模块的路径** 是相对于 **电路板目录** 的路径，即相对于 `XWOS/xwbd/WeActMiniStm32H750/` 的路径。
将其修改为：

```C
#define XWCFG_OEMPATH                           ../../../OEM
```

指向工程根目录下的 `OEM` 目录。


# 创建MyApp模块

+ 1. 在工程工程根目录下创建文件夹 `OEM` 以及 `OEM/MyApp` ，注意区分大小写。
+ 2. 在 `OEM/MyApp` 目录下创建文件 `xwmo.mk` ，此时目录结构：

```shell
├── Debug
├── DebugConfiguration
├── Doc
├── OEM
│   └── MyApp
│       └── xwmo.mk
└── XWOS

```

+ 3. 在 `OEM/MyApp/xwmo.mk` 中写入

```makefile
# 引入配置
include $(XWOS_WKSPC_DIR)/XWOS.cfg
include xwbs/functions.mk

# 增加源代码文件
XWMO_CSRCS :=

# 定义编译选项
XWMO_CFLAGS :=

# 定义头文件路径
XWMO_INCDIRS :=

# 引入编译规则
include xwbs/xwmo.mk
```

+ 4. 打开 `XWOS/xwbd/WeActMiniStm32H750/cfg/oem.h` ，增加宏定义

```C
#define OEMCFG_MyApp    1
```

+ 5. 使用STM32CubeIDE或命令 `make WKSPC=../../../Debug` 编译，
  即可在 `Debug/obj/oem/MyApp` 目录下看到一个静态库 `oem_MyApp.a` 。

+ 6. 此静态库中不包括任何代码，用户需要在 `OEM/MyApp/xwmo.mk` 增加自己的源码：

```makefile
include $(XWOS_WKSPC_DIR)/XWOS.cfg              # 包含配置文件
include xwbs/functions.mk                       # 包含xwbs定义的Makefile函数
XWMO_ASRCS :=                                   # 汇编源文件
XWMO_ASRCS_gcc :=                               # 只对gcc起作用的汇编源文件
XWMO_ASRCS_llvm :=                              # 只对llvm起作用的汇编源文件
XWMO_AFLAGS :=                                  # 汇编编译选项
XWMO_AFLAGS_gcc :=                              # 只对gcc起作用的汇编编译选项
XWMO_AFLAGS_llvm :=                             # 只对llvm起作用的汇编编译选项
XWMO_CSRCS :=                                   # C源文件
XWMO_CSRCS_gcc :=                               # 只对gcc起作用的C源文件
XWMO_CSRCS_llvm :=                              # 只对llvm起作用的C源文件
XWMO_CFLAGS :=                                  # C编译选项
XWMO_CFLAGS_gcc :=                              # 只对gcc起作用的C编译选项
XWMO_CFLAGS_llvm :=                             # 只对llvm起作用的C编译选项
XWMO_CXXSRCS :=                                 # C++源文件
XWMO_CXXSRCS_gcc :=                             # 只对gcc起作用的C++源文件
XWMO_CXXSRCS_llvm :=                            # 只对llvm起作用的C++源文件
XWMO_CXXFLAGS :=                                # C++编译选项
XWMO_CXXFLAGS_gcc :=                            # 只对gcc起作用的C++编译选项
XWMO_CXXFLAGS_llvm :=                           # 只对llvm起作用的C++编译选项
XWMO_INCDIRS := $(call getXwmoDir)              # 获取模块路径并增加到头文件搜索路径
XWMO_INCDIRS_gcc := $(call getXwmoDir)          # 获取模块路径并增加到gcc头文件搜索路径
XWMO_INCDIRS_llvm := $(call getXwmoDir)         # 获取模块路径并增加到llvm头文件搜索路径
include xwbs/xwmo.mk                            # 引用包含编译规则的Makefile
```
